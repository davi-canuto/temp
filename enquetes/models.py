from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Data de publicação')


class Choice(models.Model):
    choice_text = models.CharField(max_length=200)
    qtd_wishes = models.IntegerField('Quantidade de votos',default=0)
    question = models.ForeignKey(Question,on_delete=models.CASCADE)
